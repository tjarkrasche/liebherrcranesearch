import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraneDisplayComponent } from './crane-display.component';

describe('CraneDisplayComponent', () => {
  let component: CraneDisplayComponent;
  let fixture: ComponentFixture<CraneDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraneDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraneDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
