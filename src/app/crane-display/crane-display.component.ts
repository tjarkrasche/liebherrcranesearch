import { Component, OnInit, Input} from '@angular/core';
import {CraneDataService} from "../crane-data.service";

@Component({
  selector: 'app-crane-display',
  templateUrl: './crane-display.component.html',
  styleUrls: ['./crane-display.component.scss']
})
export class CraneDisplayComponent implements OnInit {
  radActive; weightActive; heightActive: boolean;
   get craneData(){
     return this.craneDataService.craneData
   }
  sortHelper(a, b: number, isActive: boolean):number {
    if(isActive){
      return a-b;
    }
    else {
      return -1 * (a-b)
    }
  }
  onHeightClick():void{
    this.craneDataService.craneData.sort((a,b) => {
      return this.sortHelper(a.wheelHeadHeight,b.wheelHeadHeight, this.heightActive);
    });
    this.heightActive = !this.heightActive;
    this.radActive = false;
    this.weightActive = false
  }
  onRadClick():void{
    this.craneDataService.craneData.sort((a,b) => {
      return this.sortHelper(a.radius,b.radius, this.radActive);
    });
    this.radActive = !this.radActive;
    this.heightActive = false;
    this.weightActive = false
  }
  onWeightClick():void{
    this.craneDataService.craneData.sort((a,b) => {
      return this.sortHelper(a.maxLoadCapacity,b.maxLoadCapacity, this.weightActive);
    });
    this.weightActive = !this.weightActive;
    this.radActive = false;
    this.heightActive = false
  }
  constructor(private craneDataService: CraneDataService) { }

  ngOnInit() {
  }

}
