import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  @Output() valChange = new EventEmitter<number>();

  //Input properties UI
  @Input() label: String;
  @Input() unit: String;

  // Input properties  log slider
  @Input() minPos = 0;
  @Input() maxPos = 100;
  @Input() minVal = 1;
  @Input() maxVal = 400;

  //helper Properties for logarithmic slider, shoutout: https://stackoverflow.com/questions/846221/logarithmic-slider/51671701#51671701
  minlVal; maxlVal; scale: number;

  //Default properties
  dense = true;
  disabled = false;
  min = this.minPos;
  max = this.maxPos;
  step = 1;
  discrete = false;
  markers = false;
  _position = 0;
  set position(value: number) {

    this._position = value;
    this.valChange.emit(this.value);
  }
  get position(): number {
    return this._position
  }
  //calculate value from position
  get value(): number {
    return Math.round(Math.exp((this.position - this.minPos) * this.scale + this.minlVal));
  };
  set value(value) {
    this.position =(this.minPos + (Math.log(value) - this.minlVal) / this.scale);

  }
  ngOnInit () {
    this.minlVal = Math.log(this.minVal);
    this.maxlVal = Math.log(this.maxVal);
    this.scale = (this.maxlVal-this.minlVal)/(this.maxPos-this.minPos);
  }


}
