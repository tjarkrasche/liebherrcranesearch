import { TestBed } from '@angular/core/testing';

import { CraneDataService } from './crane-data.service';

describe('CraneDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CraneDataService = TestBed.get(CraneDataService);
    expect(service).toBeTruthy();
  });
});
