import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraneFormComponent } from './crane-form.component';

describe('CraneFormComponent', () => {
  let component: CraneFormComponent;
  let fixture: ComponentFixture<CraneFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraneFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraneFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
