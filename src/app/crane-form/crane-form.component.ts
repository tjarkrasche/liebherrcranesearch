import {Component, OnInit } from '@angular/core';

import { HttpClient} from '@angular/common/http';
import {CraneDataService} from "../crane-data.service";

@Component({
  selector: 'app-crane-form',
  templateUrl: './crane-form.component.html',
  styleUrls: ['./crane-form.component.scss']
})
export class CraneFormComponent implements OnInit{

  constructor(private craneDataService: CraneDataService) {}
  get craneData() {
    return this.craneDataService.craneData;
  }
  heightChange(value: number) {
    this.craneDataService.searchHeight = value;
  }

  weightChange(value: number) {
    this.craneDataService.searchWeight = value;
  }

  radChange(value: number) {
    this.craneDataService.searchRad = value;
  }
  ngOnInit() {

  }
}
