//Angular Imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

//Blox imports
import { MaterialModule } from '@blox/material';


import { AppComponent } from './app.component';
import { SliderComponent } from './slider/slider.component';
import { CraneFormComponent } from './crane-form/crane-form.component';
import { CraneIconComponent } from './crane-icon/crane-icon.component';
import { CraneDisplayComponent } from './crane-display/crane-display.component';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
    CraneFormComponent,
    CraneIconComponent,
    CraneDisplayComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
