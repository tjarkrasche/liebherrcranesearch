import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CraneDataService {
 _searchHeight = 0;
 _searchWeight = 0;
 _searchRad = 0;
 craneData =  [];
 set searchHeight(value: number) {
   this._searchHeight = value;
   this.getCraneData();
 }
 get searchWeight() {
   return this._searchWeight
 }
  set searchWeight(value: number) {
    this._searchWeight = value;
    this.getCraneData();
  }
  get searchRad() {
    return this._searchRad
  }
  set searchRad(value: number) {
    this._searchRad = value;
    this.getCraneData();
  }
  get searchHeight() {
    return this._searchHeight
  }


 getCraneData() {
   this.http.get('https://usedapp.liebherr.com/api/Cranes?load=' + this.searchWeight +'&radius=' + this.searchRad + '&wheelHeadHeight=' + this.searchHeight + '&language=de').subscribe((data: [any])=>{this.craneData = data});
  console.log(this.http)
 }
  constructor(private http: HttpClient) { }
}
