import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraneIconComponent } from './crane-icon.component';

describe('CraneIconComponent', () => {
  let component: CraneIconComponent;
  let fixture: ComponentFixture<CraneIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraneIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraneIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
