import { Component, OnInit, EventEmitter, Output } from '@angular/core';
declare var Snap: any;
declare var mina: any;

@Component({
  selector: 'app-crane-icon',
  templateUrl: './crane-icon.component.html',
  styleUrls: ['./crane-icon.component.scss']
})
export class CraneIconComponent implements OnInit {

  @Output() newSliderSelected = new EventEmitter<number>();
  constructor() { }

  activateSlider() {

  }

  ngOnInit() {
    const s = Snap('#craneIconSvg');

    //Button Activate Höhe
    s.circle(395,58 ,9).attr({
      fill: "#fff",
      stroke: "#000"
    });
    s.circle(395,58, 5).attr({
      fill: "#000"
    });

    // Button Activate Last
    s.circle(395,230,9).attr({
      fill: "#fff",
      stroke: "#000"
    });
    s.circle(395, 230, 5).attr({
      fill: "#000"
    });

    // Button Activate Radius
    s.circle(395, 340, 9).attr({
      fill: "#fff",
      stroke: "#000"
    });
    s.circle(395, 340, 5).attr({
      fill: "#000"
    });
  }

}
